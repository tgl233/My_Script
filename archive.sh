#! /bin/bash

echo "###################打包所有.dwg文件###########################"
# 删除所有备份文件*.bak
rm -R *.bak
# 创建压缩包文件
CURRENTDATE=$(date +%Y-%m-%d__%H_%M_%S)
NAME="微机监测施工图("${CURRENTDATE}").zip"
tar -cvf ${NAME} *.dwg Record.txt 
