
"设置不备份
set nobackup
set tabstop=8 "设置tab宽为8

"定义两个脚本变量
let s:author = "唐国林"
let s:email = "<13882149056@qq.com>"
"添加修改记录函数
function! AddChangeLog()
		call append(0,"")
		call append(1,"/********************************************")
		call append(2," * 记录时间：	".strftime("%Y-%m-%d %H:%M"))
		call append(3," * 记录员:	".s:author.s:email)
		call append(4," * 修改内容：")
		call append(5," *******************************************/")
		normal gg4jo *		1.
		echo "在文件开头成功添加了1条修改记录"
		echohl WarningMsg | echo "请添加具体修改内容" | echohl None
endfunction

"把添加头文件映射到<F4>键上
"map <F4> :call AddChangeLog()
map <F4> :call AddChangeLog()<Enter>
command Addchangelog :call AddChangeLog()
