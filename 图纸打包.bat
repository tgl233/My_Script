echo off
cls
set Tool_path="C:\Program Files\Git\usr\bin\tar.exe"
echo %Tool_path%

set Name=微机监测施工图%date:~0,4%-%date:~5,2%-%date:~8,2%__%time:~0,2%_%time:~3,2%_%time:~6,2%.zip

rem 把当前目录下的所有dwg文件和record.txt打包到以时间命名的文件包中
%Tool_path% -cvf %Name% *.dwg record.txt

pause
